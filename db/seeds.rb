# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



# Some Posts
p1 = Post.create :title => "The 1st post", :body => "This is the first post"
p2 = Post.create :title => "The 2nd post", :body => "This is the second post"
p3 = Post.create :title => "The 3rd post", :body => "This is the third post"

# Comments
c1 = Comment.create :post_id => 1, :body => "Great Work!"
c2 = Comment.create :post_id => 1, :body => "Keep on!"

c3 = Comment.create :post_id => 2, :body => "Really? Are you sure?"
c4 = Comment.create :post_id => 2, :body => "I don't think so!"


